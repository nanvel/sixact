from django.conf.urls import patterns, url


urlpatterns = patterns('sixact.apps.configurator.views',
    url(r'^$', 'configurator', name='configurator'),
)