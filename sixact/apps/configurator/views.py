from django.shortcuts import render

from .forms import FinancialTemplateFormSet
from .models import FinancialTemplateCategory


def configurator(request):
    formset = FinancialTemplateFormSet(request.POST or None)
    if formset.is_valid():
        # TODO: do something
        pass
    categories = FinancialTemplateCategory.objects.all()
    return render(
            request, 'configurator/configurator.html',
            {'categories': categories})
