# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FinancialTemplate'
        db.create_table(u'configurator_financialtemplate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='templates', to=orm['configurator.FinancialTemplateCategory'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'configurator', ['FinancialTemplate'])

        # Adding unique constraint on 'FinancialTemplate', fields ['category', 'name']
        db.create_unique(u'configurator_financialtemplate', ['category_id', 'name'])

        # Adding model 'FinancialTemplateCategory'
        db.create_table(u'configurator_financialtemplatecategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal(u'configurator', ['FinancialTemplateCategory'])


    def backwards(self, orm):
        # Removing unique constraint on 'FinancialTemplate', fields ['category', 'name']
        db.delete_unique(u'configurator_financialtemplate', ['category_id', 'name'])

        # Deleting model 'FinancialTemplate'
        db.delete_table(u'configurator_financialtemplate')

        # Deleting model 'FinancialTemplateCategory'
        db.delete_table(u'configurator_financialtemplatecategory')


    models = {
        u'configurator.financialtemplate': {
            'Meta': {'unique_together': "(('category', 'name'),)", 'object_name': 'FinancialTemplate'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'templates'", 'to': u"orm['configurator.FinancialTemplateCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'configurator.financialtemplatecategory': {
            'Meta': {'object_name': 'FinancialTemplateCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['configurator']