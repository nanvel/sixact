from django import forms
from django.forms.formsets import formset_factory

from .models import FinancialTemplate


class FinancialTemplateForm(forms.ModelForm):

    class Meta:
        model = FinancialTemplate


FinancialTemplateFormSet = formset_factory(FinancialTemplateForm)
