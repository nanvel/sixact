from django.db import models


class FinancialTemplateCategory(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __unicode__(self):
        return self.name


class FinancialTemplate(models.Model):
    # TODO: extend
    category = models.ForeignKey(
            FinancialTemplateCategory, related_name='templates')
    name = models.CharField(max_length=255)

    class Meta:
        unique_together = ('category', 'name',)

    def __unicode__(self):
        return self.name
