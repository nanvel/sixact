from django.contrib import admin

from .models import FinancialTemplateCategory, FinancialTemplate


admin.site.register(FinancialTemplateCategory)
admin.site.register(FinancialTemplate)
