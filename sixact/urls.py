from django.conf.urls import patterns, include, url

from django.contrib import admin


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^configurator/', include('sixact.apps.configurator.urls')),
    url(r'^admin/', include(admin.site.urls)),
)


handler500 = 'sixact.apps.core.views.handler500'
handler404 = 'sixact.apps.core.views.handler404'
